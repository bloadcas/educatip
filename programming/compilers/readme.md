## Compiladors

Per poder fer un compilador mitjançant **flex** i **bison** podeu començar per 
instal·lar els següents programaris.

https://github.com/westes/flex/releases
http://gnuwin32.sourceforge.net/packages/flex.htm
http://gnuwin32.sourceforge.net/packages/bison.htm

Si voleu més informació sobre com implementar un compilar podeu descarregar-vos aquest pdf:

http://www.admb-project.org/tools/flex/

---

**Instal·lació**

* Aplica tots els setups dels links anteriors
* Copia de C:\Program Files (x86)\GnuWin32\lib\*.*  a C:\MinGW\lib\*   evitant sobreescriure els arxius.

---
---

## Compilers

To be able to make a compiler using ** flex ** and ** bison ** you can start with
install the following software.

https://github.com/westes/flex/releases
http://gnuwin32.sourceforge.net/packages/flex.htm
http://gnuwin32.sourceforge.net/packages/bison.htm

For more information on how to implement a compilation you can download this pdf:

http://www.admb-project.org/tools/flex/

---

** Installation **

* Applies all setups of previous links
* Copy C: \ Program Files (x86) \ GnuWin32 \ lib \ *. * To C: \ MinGW \ lib \ * avoids overwriting the files.